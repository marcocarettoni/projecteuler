import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Launcher {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		long start = System.currentTimeMillis();

//		runCompleted();
//		runToOptimize();
		runInProgress();

		Log.debug((System.currentTimeMillis() - start) + " ms", Launcher.class);
	}

	private static void runInProgress() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		((Project) Class.forName("projecteuler.Problem0029").newInstance()).printResult();
	}

	@SuppressWarnings("unused")
	private static void runToOptimize() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		((Project) Class.forName("projecteuler.Problem0026").newInstance()).printResult();
	}

	@SuppressWarnings("unused")
	private static void runCompleted() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		((Project) Class.forName("projecteuler.Problem0001").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0002").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0003").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0004").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0005").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0006").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0007").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0008").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0009").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0010").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0011").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0012").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0013").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0014").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0015").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0016").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0017").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0018").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0019").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0020").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0021").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0022").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0023").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0024").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0025").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0027").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0028").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0029").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0037").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0048").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0052").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0059").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0067").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0089").newInstance()).printResult();
		((Project) Class.forName("projecteuler.Problem0493").newInstance()).printResult();
	}

}

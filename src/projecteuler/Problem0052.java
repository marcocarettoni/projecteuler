/**
 * Permuted multiples
Problem 52
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0052 implements Project {

	@Override
	public void printResult() {
		int how = 123456;
		while (!checkFound(how++)) {}
	}

	private boolean checkFound(int num) {
		String num1 = Integer.toString(num);

		String num6 = Integer.toString(num * 6);
		if (num1.length() != num6.length())
			return false;
		
		String num5 = Integer.toString(num * 5);
		if (num1.length() != num5.length())
			return false;
		
		String num4 = Integer.toString(num * 4);
		if (num1.length() != num4.length())
			return false;
		
		String num3 = Integer.toString(num * 3);
		if (num1.length() != num3.length())
			return false;

		String num2 = Integer.toString(num * 2);
		if (num1.length() != num2.length())
			return false;

		for (int i = 0; i < num1.length(); i++) {
			String car = num1.charAt(i) + "";

			if (num2.indexOf(car) == -1 || num3.indexOf(car) == -1 || num4.indexOf(car) == -1 ||
					num5.indexOf(car) == -1 || num6.indexOf(car) == -1)
				return false;
		}
		
		Log.result(num1 + " - " + num2 + " - " + num3 + " - " + num4 + " - " + num5 + " - " + num6, this.getClass());
		return true;
	}
}

/**
 * Special Pythagorean triplet
Problem 9
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
 */
package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0009 implements Project {

	@Override
	public void printResult() {
		
		for (int i = 0 ; i < 500 ; i++)
		{
			for (int j = 0 ; j < 500 ; j++)
			{
				double sqr = Math.sqrt((i*i) + (j*j));
				double ris = i + j + sqr;
				if (ris == 1000)
				{
					Log.result("A: " + i + " B: " + j + " C: " + sqr + " - PROD: " + ((long)i*j*(long)sqr) + "", this.getClass());
					return;
				}
				if (ris > 1000)
				{
					break;
				}
			}
		}
	}
}

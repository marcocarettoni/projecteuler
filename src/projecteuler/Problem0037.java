/**
 * Truncatable primes
Problem 37
The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
 */

package projecteuler;

import java.util.Arrays;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0037 implements Project {

	private static final int MAX_NUMBER = 1000000;
	boolean [] primes = new boolean[MAX_NUMBER];

	@Override
	public void printResult() {
		
		int how = (int) Math.sqrt(MAX_NUMBER);
		Arrays.fill(primes, true);
		primes[1] = false;
		for (int i = 2 ; i <= how ; i++)
		{
			int cont = MAX_NUMBER / i;
			if (primes[i])
			{
				for (int k = 2 ; k < cont ; k++)
				{
					primes[i*k] = false;
				}
			}
		}
		
		int sum = 0, count = 0;	
		for (int i = 11 ; i < MAX_NUMBER ; i = i + 2)
		{
			int divisor = 1;
			while(i / divisor >= 1)
			{
				if (!primes[i%divisor] || !primes[i/divisor])
				{
					divisor = 1;
					break;
				}
				divisor *= 10; 
			}
			if (divisor > 1)
			{
				sum += i;
				count++;
			}
		}

		Log.result("Result: " + sum + " of " + count + " primes" + "", this.getClass());
	}
}

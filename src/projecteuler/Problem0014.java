/**
 * Longest Collatz sequence
Problem 14
The following iterative sequence is defined for the set of positive integers:

n -> n/2 (n is even)
n -> 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
 */
package projecteuler;
import java.util.HashMap;
import java.util.Map;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0014 implements Project {

	public static final long MAX_NUMBER = 1000000L; 
	
	@Override
	public void printResult() {
		long startingNumber = (MAX_NUMBER / 2) + 1;
		Map<Long, Long> maps = new HashMap<Long, Long>();
		maps.put(1L, 1L);
		long number = 0;
		long max = 0;
		for (long i = startingNumber ; i < MAX_NUMBER; i++) {

			long how = 0;
			long num = i;
			while (num != 1) {
				how++;
				if (num % 2 == 0) {
					num = num / 2;
				} else {
					num = (3 * num) + 1;
				}
				
				if (maps.containsKey(num)) {
					how += maps.get(num);
					break;
				}
			}
			if (how > max) {
				number = i;
				max = how;
			}
			maps.put(i, how);
		}

		Log.result("Number: " + number + " Len: " + max + "", this.getClass());
	}
}

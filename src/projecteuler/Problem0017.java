/**
 * Number letter counts
Problem 17
If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?


NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
 */
package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0017 implements Project{

	@Override
	public void printResult() {
		int sum = 0;
		for (int i = 1 ; i <= 1000 ; i++) {
			sum += getLengthWord(i);
		}
		
		Log.result(sum + "", this.getClass());
	}

	private int getLengthWord(int n) {
		
		String word = "";
		int num = n % 1000;
		n = ((n-num)/1000);
		if (n > 0 && n < 20){word += getUnit(n)+"thousand";}
		n = num;
		num = n % 100;
		n = ((n-num)/100);
		if (n > 0 && n < 20){word += getUnit(n)+"hundred";
		 if (num > 0) word += "and";
		}
		
		if (num < 20){
			word += getUnit(num);
		}
		else
		{
			n = num;
			num = n % 10;
			n = ((n-num)/10);
			word += getDecimal(n) + getUnit(num);
		}
		return word.length();
	}
	
	private String getUnit(int num)
	{
		switch (num)
		{
			case 1:  
			case 2:  
			case 6:  
			case 10: return "ten";
			
			case 4: 
			case 5:
			case 9: return "nine";
			
			case 3: 
			case 7:
			case 8: return "eight";
			
			case 11:
			case 12: return "twelve";
			
			case 15:
			case 16: return "sixteen";
			
			case 13:
			case 14: 
			case 18:
			case 19: return "nineteen";
			
			case 17: return "seventeen";
		}
		return "";
	}
	
	private String getDecimal(int num)
	{
		switch (num)
		{
			case 4: 
			case 5:
			case 6: return "sixty";
		
			case 2:
			case 3:
			case 8:
			case 9: return "ninety";
			
			case 7: return "seventy";

		}
		return "";
	}
}

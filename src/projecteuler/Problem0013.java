/**
 * Large sum
	Problem 13
	Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
 */
package projecteuler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0013 implements Project {

	
	
	@Override
	public void printResult() {
		BufferedReader reader = null;
		try{		 
		    reader = new BufferedReader(new FileReader(new File("./allegati/Problem13.txt")));

		    String line;
		    BigInteger sum = BigInteger.valueOf(0);
		    while ((line = reader.readLine()) != null) {
		    	sum = sum.add(new BigInteger(line));
		    }
		    
		    Log.result(sum.toString().substring(0, 10), this.getClass());
		} catch (IOException e) {
		    Log.error(e.getMessage(), this.getClass());
		} finally {
		    try {
		        reader.close();
		    } catch (IOException e) {
		    	Log.error(e.getMessage(), this.getClass());
		    }
		}
	}
}

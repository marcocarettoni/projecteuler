/**
 * Smallest multiple
 * Problem 5
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * 
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0005 implements Project {

	@Override
	public void printResult() {
		int []arr = new int[20];
		for (int j = 1 ; j < 21 ; j++)
		{
			int num = j;
			int lastFound = 0;
			int molt = 1;
			for (int i = 2 ; i <= j ; i++)
			{
				if (num % i == 0)
				{
					num = num / i;
					if (lastFound != i)
					{
						if (lastFound > 0 && arr[lastFound] < molt)
						{
							arr[lastFound-1] = molt;
						}
						lastFound = i;
						molt = 1;
					}
					else
					{
						molt++;
					}
					i--;
				}
			}
			
			if (lastFound > 0 && arr[lastFound] < molt)
			{
				arr[lastFound-1] = molt;
			}
			
			if (lastFound == 0 && arr[j-1] <= j)
			{
				arr[j-1] = j;
			}
		}
		
		int mult = 1;
		for (int i = 0 ; i < arr.length ; i++)
		{
				mult = mult * (int)Math.pow(i+1, arr[i]);
		}
		Log.result(mult + "", this.getClass());
	}

}

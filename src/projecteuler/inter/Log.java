package projecteuler.inter;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Log {

	public static final String RESULT = "RESULT";
	public static final String INFO = "INFO";
	public static final String DEBUG = "DEBUG";
	public static final String ERROR = "ERROR";
	public static final String WARN = "WARN";
	
	public static void echo(String s)
	{
		echo(s, DEBUG);
	}
	
	public static void echo(String s, String TYPE)
	{
		echo(s, DEBUG, null);
	}
	
	public static void echo(String s, String TYPE, Class<?> CLASSE)
	{
		String print = "[" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new GregorianCalendar().getTime()) + "]["+TYPE+"]";
		
		if (CLASSE != null)
		{
			print += "["+CLASSE.getName()+"]";
		}
		print += " " + s;
		System.out.println(print);
	}
	
	public static void result(String s, Class<?> CLASSE)
	{
		echo(s, RESULT, CLASSE);
	}
	
	public static void error(String s, Class<?> CLASSE)
	{
		echo(s, ERROR, CLASSE);
	}
	
	public static void debug(String s, Class<?> CLASSE)
	{
		echo(s, DEBUG, CLASSE);
	}
	
	public static void info(String s, Class<?> CLASSE)
	{
		echo(s, INFO, CLASSE);
	}
}
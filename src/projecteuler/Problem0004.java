/**
 *	Largest palindrome product
 *	Problem 4
 *	A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.
 *	
 *	Find the largest palindrome made from the product of two 3-digit numbers.
*/

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0004 implements Project {

	@Override
	public void printResult() {
		int max = 0;
		for (int j = 999 ; j > 0 ; j--)
		{
			for (int i = 999 ; i > 0 ; i--)
			{
				if (checkPalindrome(i*j))
				{
					if (max < (i*j))
						max = i*j;
				}
			}
		}
		

		Log.result(max + "", this.getClass());
	}
	
	private boolean checkPalindrome(int num2)
	{
		String num = num2+"";
		if ( num.length()%2 == 1 )
		{
			return false;
		}
		
		int middle = (num.length()/2);
		for (int i = 0 ; i < middle ; i++)
		{
			if (num.charAt((middle+i)) != num.charAt((middle-i-1)))
			{
				return false;
			}
		}
		
		return true;
	}

}

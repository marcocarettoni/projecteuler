/** 
 * Maximum path sum II
Problem 67
By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.

NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)
 *  */

package projecteuler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0067 implements Project {

	
	public void printResult() {
		
		Integer tri[] = new Integer[10];
		
		BufferedReader reader = null;
		try{		 
		    reader = new BufferedReader(new FileReader(new File("./allegati/Problem67.txt")));

		    String line;
		    ArrayList<Integer> lista = new ArrayList<Integer>();
		    while ((line = reader.readLine()) != null) {
		    	
		    	String [] numbs = line.split(" ");
		    	for (int i = 0 ; i < numbs.length ; i++)
		    	{
		    		if (numbs[i].replaceAll(" ", "").length() > 0)
		    		{
		    			lista.add(Integer.parseInt(numbs[i]));
		    		}
		    	}
		    }
		    
		   tri = lista.toArray(tri);
		} catch (IOException e) {
		    Log.error(e.getMessage(), this.getClass());
		} finally {
		    try {
		        reader.close();
		    } catch (IOException e) {
		    	Log.error(e.getMessage(), this.getClass());
		    }
		}
		
		int len  = tri.length / 1;
	    int base = (int)(Math.sqrt(8*len + 1) - 1) / 2;
	    int step = base - 1;
	    int stepc  = 0;
	 
	    int i;
	    for (i = len - base - 1; i >= 0; --i) {
	        tri[i] += tri[i + step] > tri[i + step + 1] ? tri[i + step] : tri[i + step + 1];
	        if (++stepc == step) {
	            step--;
	            stepc = 0;
	        }
	    }
		
		Log.result("Result: " + tri[0], this.getClass());
	}
	

}

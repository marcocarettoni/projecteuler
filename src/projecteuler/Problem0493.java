/**
 * Under The Rainbow
Problem 493
70 colored balls are placed in an urn, 10 for each of the seven rainbow colors.

What is the expected number of distinct colors in 20 randomly picked balls?

Give your answer with nine digits after the decimal point (a.bcdefghij).
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0493 implements Project {

	@Override
	public void printResult() {
		pow[0] = 1;
		for(int i = 1; i <= numcol; i++){
			pow[i] = (maxcol+1)*pow[i-1];
		}
		double store[] = new double[pow[numcol]];
		for(int i = 0; i < numcol ; i++){
			store[pow[i]]=1.0/((double)numcol);
		}
		for(int r = 1; r < topick; r++){
			for(int i = 1; i < pow[numcol]; i++){
				if(store[i] > 0.0 && pickedSoFar(i) == r){
					for(int c = 0; c < numcol; c++){
						if(pickedOfCol(c, i) < maxcol){
							double pc = (double)(maxcol - pickedOfCol(c, i))/(double)(numcol*maxcol-r);
							store[i+pow[c]] += pc*store[i];
						}
					}
				}
			}
		}
		double result = 0.0;
		for(int i = 1; i < pow[numcol]; i++){
			if(pickedSoFar(i) == topick){
				result+=(double)(pickedUnique(i))*store[i];
			}
		}
		Log.result("Expected value: " + result, this.getClass());
	}
	
	static int numcol = 7; //number of different colours
	static int maxcol = 10; //how many balls of each colour
	static int topick = 20; //number of balls to draw
	static int[] pow = new int[numcol+1];

	static int pickedSoFar(int n){
		int ret = 0;
		for(int i = 0; i < numcol; i++){
			ret+= pickedOfCol(i, n);
		}
		return ret;
	}
	static int pickedOfCol(int c, int n){
		return ((n/pow[c])%(maxcol+1));
	}
	static int pickedUnique(int n){
		int ret = 0;
		for(int i = 0; i < numcol; i++){
			if((pickedOfCol(i, n)) > 0) ret++;
		}
		return ret;
	}

}

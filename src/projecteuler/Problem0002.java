/**
 * Even Fibonacci numbers
 * Problem 2
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:
 * 
 * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * 
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0002 implements Project {

	@Override
	public void printResult() {
		long sum = 0;
		long seq1 = 1, seq2 = 1;
		do{
			if (seq1 % 2 == 0)
				sum += seq1;
			long tmp = seq1;
			seq1 += seq2;
			seq2 = tmp;
		}while(seq1 < 4000000);
		
		Log.result(sum+"", this.getClass());
	}
}

/**
 * 10001st prime
Problem 7
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
 */
package projecteuler;

import java.util.Arrays;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0007 implements Project {

	private static final int MAX_NUMBER = 1000000;
	boolean [] primes = new boolean[MAX_NUMBER];

	@Override
	public void printResult() {
		
		int how = (int) Math.sqrt(MAX_NUMBER);
		int count = 0;
		Arrays.fill(primes, true);
		primes[1] = false;
		for (int i = 2 ; i <= how ; i++)
		{
			int cont = MAX_NUMBER / i;
			if (primes[i])
			{
				for (int k = 2 ; k < cont ; k++)
				{
					primes[i*k] = false;
				}
			}
		}
		
		for (int i = 2 ; i <= MAX_NUMBER ; i++)
		{
			if (primes[i])
				count++;
			
			if (count == 10001)
			{
				Log.result("Result: " + i, this.getClass());
				break;
			}
		}

	}

}

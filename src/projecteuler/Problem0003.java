/**
 * Largest prime factor
 * Problem 3
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0003 implements Project {

	private static final long NUMBER = 600851475143L;
	
	@Override
	public void printResult() {
		
		int max = 0;
		int how = (int) Math.sqrt(NUMBER);
		for (int i = 3 ; i <= how ; i+=2)
		{
			if (!(i % 3 == 0 || i % 5 == 0 || i % 7 == 0) && 
					checkIsPrime(i) && 
					NUMBER % i == 0 && 
					max < i)
			{
				max = i;
			}
		}
		
		Log.result(max + "", this.getClass());
	}
	
	private boolean checkIsPrime(int num)
	{
		int middle = (int) Math.sqrt(num) + 1;
		for (int i = 3 ; i <= middle ; i=i+2)
		{
			if (num % i == 0)
			{
				return false;
			}			
		}
		return true;
	}
}

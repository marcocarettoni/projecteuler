/**
 * Reciprocal cycles
Problem 26
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
 */
package projecteuler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0026 implements Project{

	@Override
	public void printResult() {
		Log.result(maxRecurringDecimal() + "", this.getClass());
	}
	
	private int maxRecurringDecimal()
	{
		BigDecimal num = new BigDecimal(0);
		int max = 2;
		int where = 0;
		for (int i = 3 ; i < 1000 ; i += 2)
		{
			num = new BigDecimal(1).divide(BigDecimal.valueOf(i), 2000, BigDecimal.ROUND_HALF_UP);
			
			int tmp = checkRecurring(num.toPlainString());
			if (tmp > max)
			{
				max = tmp;
				where = i;
			}
		}
		return where;
	}
	
	private int checkRecurring(String number)
	{
		List<String> li = null;
		number = number.replace("0.", "");
		for (int i = 2 ; i < (number.length() / 2) ; i++)
		{
			li = new ArrayList<String>(Arrays.asList(number.split(number.substring(0, i))));
			li.removeAll(Arrays.asList(""));
			if (li.size() == 0 || (li.size() == 1 && li.get(0).length() < i && li.get(0).equals(number.substring(0, li.get(0).length()))))
			{
				return i;
			}
		}
		return 0;
	}
}

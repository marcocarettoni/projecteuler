/**
 * Number spiral diagonals
Problem 28
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
 */
package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0028 implements Project {

	@Override
	public void printResult() {
		long sum = 1;
		long val = 1;
		for (long i = 2; i < 1001 ; i=i+2) {
			val += i; sum += val;
			val += i; sum += val;
			val += i; sum += val;
			val += i; sum += val;
		}
		
		Log.result(sum + "", this.getClass());
	}
}

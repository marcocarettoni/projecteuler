/**
 * Amicable numbers
Problem 21
Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a != b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
 */
package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0021 implements Project{

	@Override
	public void printResult() {
		long sum = 0;
		for (long i = 1; i < 10000; i++) {
			sum += isAmicable(i);
		}

		Log.result(sum + "", this.getClass());
	}

	private long isAmicable(long n) {

		long num = sumDivisor(n);
		long num2 = sumDivisor(num);
		if (num < 10000 && n == num2 && n != num) {
			return n;
		}
		return 0;
	}

	private long sumDivisor(long n) {
		long tot = 1;
		long middle = (long) Math.sqrt(n);//n / 2;
		for (int i = 2; i <= middle; i++) {
			if (n % i == 0)
				tot += i + (n / i);
		}

		return tot;
	}
}

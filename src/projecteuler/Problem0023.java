/**
 * Non-abundant sums
Problem 23
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
 */
package projecteuler;

import java.util.Arrays;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0023 implements Project {

	@Override
	public void printResult() {
		Log.result("Result: " + getResult(), this.getClass());
	}

	private static final int MAX_SIZE = 9999999;

	private String getResult() {
		boolean[] list = new boolean[MAX_SIZE];
		list[0] = false;
		list[1] = false;
		list[2] = false;
		for (int i = 3; i < MAX_SIZE; i++) {
			list[i] = isAbundant(i);
		}

		boolean[] list2 = new boolean[MAX_SIZE];
		Arrays.fill(list2, false);
		for (int i = 0; i < list.length; i++) {
			for (int j = 0; j < list.length; j++) {
				if (i + j < MAX_SIZE) {
					if (list[i] && list[j])
						list2[i + j] = true;
				} else
					break;
			}
		}

		int sum = 0;
		for (int i = 0; i < list2.length; i++) {
			if (!list2[i])
				sum += i;
		}
		return sum + "";
	}

	private boolean isAbundant(int num) {

		int sqrt = (int) Math.sqrt(num);
		int sum = 1;
		for (int i = 2; i <= sqrt; i++) {
			if (num % i == 0) {
				sum += i + (num / i == i ? 0 : num / i);
			}
			if (sum > num) {
				return true;
			}
		}
		return sum > num;
	}
}

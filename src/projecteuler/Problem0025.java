/**
 * 1000-digit Fibonacci number
Problem 25
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn-1 + Fn-2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the first term in the Fibonacci sequence to contain 1000 digits?
 */
package projecteuler;

import java.math.BigInteger;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0025 implements Project{

	@Override
	public void printResult() {
		Log.result(fibonacci(new BigInteger("1"), new BigInteger("1"), 3) + "", this.getClass());
	}
	
	private int fibonacci(BigInteger f1, BigInteger f2, int lvl)
	{
		BigInteger f3 = new BigInteger("0").add(f1).add(f2);			
		if (f3.toString().length() >= 1000)
		{		
			return lvl;
		}		
		return fibonacci(f2,f3, ++lvl);
	}
}

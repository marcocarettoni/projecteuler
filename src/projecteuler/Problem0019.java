/** 
 * Counting Sundays
Problem 19
You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0019 implements Project {

	
	public void printResult() {
		int [] monthsDay = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int lastMonth = 0,totsun = 0;
		for (int i = 1900 ; i < 2001 ; i++)
		{			
			for (int j = 0 ; j < 12 ; j++)
			{				
				totsun +=((lastMonth = ((lastMonth+(((i%100==0&&i%400==0)||(i%4==0&&i%100!=0)?true:false) && monthsDay[j] == 28?monthsDay[j]:29))%7))== 6 && i > 1900 && !(j == 11 && i == 2000)?1:0);				
			}
		}
		
		Log.result(totsun + "", this.getClass());
	}
	
	public int a(int i, int l){
		return ((l=((l+((((i-(i%12)/12)%100==0&&(i-(i%12)/12)%400==0)||((i-(i%12)/12)%4==0&&(i-(i%12)/12)%100!=0)?true:false)&&new int[]{31,30,31,30,31,31,30,31,30,31,28,31}[12-((i%12)+1)]== 28?new int[]{31,30,31,30,31,31,30,31,30,31,28,31}[12-((i%12)+1)]:29))%7))== 6&&(i-(i%12)/12)>0&&!(i%12==11&&(i-(i%12)/12)==100)?1:0)+(i>1200?0:a(++i,l));
	}

}

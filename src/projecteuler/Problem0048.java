/**
 * Self powers
Problem 48
The series, 11 + 22 + 33 + ... + 1010 = 10405071317.

Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.
 */

package projecteuler;
import java.math.BigInteger;

import projecteuler.inter.Log;
import projecteuler.inter.Project;


public class Problem0048 implements Project{
	
	@Override
	public void printResult()
	{
		BigInteger num = new BigInteger("0");
		for (int i = 1000; i > 0 ; i--)
		{
			num = num.add(new BigInteger(Integer.toString(i)).pow(i));
		}
		String val = num.toString();
		
		Log.result((val.substring(val.length() - 10)) + "", this.getClass());
	}
	

}
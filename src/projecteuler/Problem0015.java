/**
 * Lattice paths
Problem 15
Starting in the top left corner of a 2�2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20�20 grid?
 */
package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0015 implements Project {

	@Override
	public void printResult() {
		// Binomial coefficient
		// (20+20)! / ( 20! * (40-20)!)

		double rows = 20;
		double cols = 20;

		double result = (double) fattoriale(rows + cols) / (fattoriale(rows) * fattoriale(cols));
		Log.result("Result: " + result, this.getClass());
	}

	private double fattoriale(double num) {
		if (num == 1)
			return 1;

		return num * fattoriale(--num);
	}
}

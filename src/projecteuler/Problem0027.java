/**
 * Quadratic primes
Problem 27
Euler discovered the remarkable quadratic formula:

n� + n + 41

It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when n = 41, 41� + 41 + 41 is clearly divisible by 41.

The incredible formula  n� - 79n + 1601 was discovered, which produces 80 primes for the consecutive values n = 0 to 79. The product of the coefficients, -79 and 1601, is -126479.

Considering quadratics of the form:

n� + an + b, where |a| < 1000 and |b| < 1000

where |n| is the modulus/absolute value of n
e.g. |11| = 11 and |-4| = 4
Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.
 */
package projecteuler;

import java.util.Arrays;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0027 implements Project {

	private static final int MAX_NUMBER = 2000999;
	boolean[] primes = new boolean[MAX_NUMBER];

	@Override
	public void printResult() {
		Log.result(getProducts() + "", this.getClass());
	}

	private int getProducts() {
		int how = (int) Math.sqrt(MAX_NUMBER);

		Arrays.fill(primes, true);
		primes[1] = false;
		for (int i = 2; i <= how; i++) {
			int cont = MAX_NUMBER / i;
			if (primes[i]) {
				for (int k = 2; k < cont; k++) {
					primes[i * k] = false;
				}
			}
		}

		int max = 0;
		int prod = 0;
		for (int a = -999; a < 1000; a++) {
			for (int b = -999; b < 1000; b++) {
				for (int i = 0; i < 1000; i++) {
					int num = ((i * i) + (a * i) + b);
					if (num < 0 || !primes[num]) {
						if (i > max) {
							max = i;
							prod = a * b;
						}
						break;
					}
				}

			}
		}
		return prod;
	}

}

/**
 * Summation of primes
Problem 10
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
 */
package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0010 implements Project {

	@Override
	public void printResult() {
		long sum = 2+3;
		for (int i = 5 ; i < 2000000 ; i=i+2)
		{
			if (checkIsPrime(i))
			{
				sum += i;
			}
		}
		Log.result(sum + "", this.getClass());
	}
	
	private boolean checkIsPrime(int num)
	{	
		int middle = (int) Math.sqrt(num) + 1;
		for (int i = 3 ; i <= middle ; i=i+2)
		{
			if (num % i == 0)
			{
				return false;
			}			
		}
		return true;
	}

}

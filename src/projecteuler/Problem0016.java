/**
 * Power digit sum
Problem 16
215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 21000?
*/
package projecteuler;
import java.math.BigInteger;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0016 implements Project {

	@Override
	public void printResult() {
		
		BigInteger num = BigInteger.valueOf(2).pow(1000);
		int totale = 0;
		for (int i = 0 ; i < num.toString().length() ; i++)
		{
			totale += Integer.parseInt(num.toString().charAt(i)+"");
		}
		Log.result("Result: " + totale, this.getClass());
	}
}

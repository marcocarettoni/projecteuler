/**
 * Sum square difference
Problem 6
The sum of the squares of the first ten natural numbers is,

1^2 + 2^2 + ... + 10^2 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)^2 = 55^2 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0006 implements Project {

	@Override
	public void printResult() {
		long powSum = 0L;
		long sum = 0L;
		for (int i = 1 ; i <= 100 ; i++)
		{
			powSum = powSum + (long) Math.pow(i, 2);
			sum = sum + i;
		}
		Log.result(((long)Math.pow(sum, 2) - powSum) + "", this.getClass());
	}

	
}
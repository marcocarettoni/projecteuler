/**
 * Lexicographic permutations
Problem 24
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
 */
package projecteuler;

import java.util.ArrayList;
import java.util.Arrays;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0024 implements Project {

	private static final int NTH_NUBER_PERMUTATION = 1000000;
	@Override
	public void printResult() {
		Log.result("Result: " + getPermutation(), this.getClass());
	}

	private String getPermutation()
	{
		ArrayList<Integer> arr = new ArrayList<Integer>();		
		arr.addAll(Arrays.asList(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));
		int target = NTH_NUBER_PERMUTATION - 1;
		String str = "";
		for (int i = arr.size() ; i > 0 ; i--)
		{
			double fatt = fattoriale(i-1);
			int pos = (int) (target / fatt);
			target = (int) (target % fatt);
			str += "" + arr.get(pos);
			arr.remove(pos);
		}
		
		return str;
	}
	
	private double fattoriale(double num) {
		if ((int) num <= 1) return 1;
		
		return num * fattoriale(--num);
	}
}

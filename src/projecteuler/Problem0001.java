/**
 * Multiples of 3 and 5
 * Problem 1
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * 
 * Find the sum of all the multiples of 3 or 5 below 1000.
 *
 */

package projecteuler;

import projecteuler.inter.Log;
import projecteuler.inter.Project;

public class Problem0001 implements Project {

	@Override
	public void printResult() {
		long sum = 0;
		int maxLoop = 1000 / 3;
		for (int i = 1 ; i <= maxLoop ; i++)
		{
			if (i * 5 < 1000) 
				sum += (i*5);
			
			if (i % 5 != 0)
				sum += i * 3;
		}
		
		Log.result(sum+"", this.getClass());
	}
	
	
}
